package com.blanclabs.ecorp.security.test.ecorp.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blanclabs.ecorp.security.test.ecorp.utils.Utils;

public class UtilsTest {

	@Test
	public void calculateFactorial() {
		int sixFactorial = 6 * 5 * 4 * 3 * 2 * 1;
		assertEquals(sixFactorial, Utils.calculateFactorial(6));
	}

	@Test
	public void calculateFactorialWhenZero() {
		assertEquals(1, Utils.calculateFactorial(0));
	}

}

package com.blanclabs.ecorp.security.test.ecorp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blanclabs.ecorp.security.test.ecorp.model.PredictorModel;

@RequestMapping("/ecorp")
@RestController
public class CalculateFakeCardsController {

	private static final Log LOG = LogFactory.getLog(CalculateFakeCardsController.class);

	@Autowired
	private PredictorModel predictorModel;

	@RequestMapping(value = "calculate-fake-cards/{days}", method = RequestMethod.GET)
	public long calculateNumberFakeCards(@PathVariable("days") long days) {
		LOG.info("Getting number of fake credit cards for the day: " + days);
		long fakeCards = predictorModel.calculateFakeCards(days);
		LOG.info("The number of fake cards are: " + fakeCards);
		return fakeCards;
	}
}

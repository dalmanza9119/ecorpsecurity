package com.blanclabs.ecorp.security.test.ecorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcorpApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcorpApplication.class, args);
	}
}

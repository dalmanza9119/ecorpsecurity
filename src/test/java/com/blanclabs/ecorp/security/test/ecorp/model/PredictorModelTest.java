package com.blanclabs.ecorp.security.test.ecorp.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = { PredictorModel.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class PredictorModelTest {

	@Autowired
	PredictorModel predictorModel;

	@Test
	public void calculateFakeCards() {
		long expectedCards5thDay = 600;
		assertEquals(expectedCards5thDay, predictorModel.calculateFakeCards(5));
	}

	@Test
	public void calculateFakeCardsZeroDays() {
		long expectedCards0thDay = 0;
		assertEquals(expectedCards0thDay, predictorModel.calculateFakeCards(0));
	}

}

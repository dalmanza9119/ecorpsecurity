package com.blanclabs.ecorp.security.test.ecorp.model;

import org.springframework.stereotype.Component;

import com.blanclabs.ecorp.security.test.ecorp.utils.Utils;

@Component
public class PredictorModel {

	public long calculateFakeCards(long days) {
		return days * Utils.calculateFactorial(days);
	}

}

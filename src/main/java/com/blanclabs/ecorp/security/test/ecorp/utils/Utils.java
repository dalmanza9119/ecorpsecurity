package com.blanclabs.ecorp.security.test.ecorp.utils;

public class Utils {

	public static long calculateFactorial(long number) {
		if (number <= 1)
			return 1;
		else
			return number * calculateFactorial(number - 1);
	}
}

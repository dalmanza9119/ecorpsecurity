package com.blanclabs.ecorp.security.test.ecorp.controller;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
public class CalculateFakeCardsControllerTest {

	private static final String NUMBER_OF_CARDS_EXPECTED_6_DAY = "4320";
	private static final String URI_CALCULATE_FAKE_CARDS = "/calculate-fake-cards/";
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void calculateNumberFakeCardsTest() {
		try {
			mockMvc.perform(get(URI_CALCULATE_FAKE_CARDS + "/6")).andExpect(status().isOk())
					.andExpect(content().string(NUMBER_OF_CARDS_EXPECTED_6_DAY));
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void calculateNumberFakeCardsTest404() {
		try {
			mockMvc.perform(get(URI_CALCULATE_FAKE_CARDS)).andExpect(status().isNotFound());
		} catch (Exception e) {
			fail();
		}
	}

}
